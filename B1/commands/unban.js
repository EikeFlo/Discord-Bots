/**
 * @param {Discord.Client} bot
 * @param {Discord.Message} message
 * @param {array} args
 */

const Discord = require("discord.js");

module.exports.run = async (bot, message, args) => {
  if(!message.member.hasPermission(["BAN_MEMBERS","ADMINISTRATOR"])) return message.channel.send("NOPE")

  //let bUser = message.guild.member(message.mentions.users.first() || message.guild.members.get(args[0])); // Normal function for ban
  let bUser = await bot.fetchUser(args[0])
      if(!bUser) return message.channel.send("I need a user fucker")
    

  let bReason = args.slice(1).join(" ")
      if(!bReason) bReason = "No Reason provided" 

  if(!message.guild.me.hasPermission(["BAN_MEMBERS","ADMINISTRATOR"])) return message.channel.send("NOPE")
    message.delete()
    try{
      message.guild.unban(bUser,{reason: bReason})
      message.channel.send(`${bUser.tag} has been unbanned`)
    }catch(e){
      console.log(e.message)
    }
    if(!bReason) bReason = "No Reason "
    let unbanembed = new Discord.RichEmbed()
    
  .setDescription("~unban~")
  .setColor("#bc0000")
  .addField("unbanned User", `${bUser}`)
  .addField("unbanned By", `<@${message.author.id}>`)
  .addField("unbanned In", message.channel)
  .addField("Time", message.createdAt)
 
  .addField("Reason", bReason);
  
  let incidentchannel = message.guild.channels.find(`name`, "incidents");
  if(!incidentchannel) return message.channel.send("Can't find incidents channel.");
  incidentchannel.send(unbanembed);

 

}
    module.exports.help = {
    name: "unban"
  }