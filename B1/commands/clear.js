/**
 * @param {Discord.Client} bot
 * @param {Discord.Message} message
 * @param {array} args
 */const Discord = require("discord.js");

module.exports.run = async (bot, message, args) => {
  message.delete()
    let messagecount = parseInt(args);
    message.channel.fetchMessages({ limit: messagecount })
      .then(messages => message.channel.bulkDelete(messages))
      
}

module.exports.help = {
  name: "clear"
}